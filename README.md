## Android Project – Gapt

### What is this application?
This app is built to assist colour blind people when performing object detection and colour detection.

### Object Detection
Point the camera to an object for the object to be detected. This was implemented with the help of CameraX and ML Kit. Photos can also be taken in this mode. 
https://developer.android.com/training/camerax

### Colour Detection
Point the camera to an object, tap the screen, and the colour will be displayed in the top left corner alongside it being in text form in the middle of the screen. Text to speech is also an implemented feature where the user can have the application read the detected colour out loud.  This was achieved by using the OpenCV library and some implementations from Color-Friend.
http://opencv.org/
https://github.com/abhi-sai/Color-Friend#readme

### Ishihara test
The Ishihara test is a colour perception test used to detect red-green colour deficiencies. It was first published in 1917 by Dr. Shinobu Ishihara, a professor at the University of Tokyo.
Note: A doctor should be consulted if needed as this test may not be fully accurate.

### Colour Settings
Allows the user to access their phone’s accessibility settings.
