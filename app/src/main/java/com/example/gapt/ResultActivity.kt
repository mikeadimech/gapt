package com.example.gapt

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        val totalQuestions = intent.getIntExtra(Constants.TOTAL_QUESTIONS, 0)
        val correctAnswers = intent.getIntExtra(Constants.CORRECT_ANSWERS, 0)
        val protanopia = intent.getIntExtra(Constants.PROTANOPIA_ANSWERS, 0)
        val deuteranopia = intent.getIntExtra(Constants.DEUTERANOPIA_ANSWERS, 0)

        val button1 = findViewById<Button>(R.id.btn_finish)
        val layoutView = findViewById<LinearLayout>(R.id.container)
        val textView1 = findViewById<TextView>(R.id.tv_result)
        val textView2 = findViewById<TextView>(R.id.tv_score)

        if(Global.sight == "Protanopia"){
            button1.setTextColor(Global.BLACK.toInt())
            layoutView.setBackgroundColor(Global.LIGHT_YELLOW.toInt())
            textView1.setTextColor(Global.BLACK.toInt())
            textView2.setTextColor(Global.BLACK.toInt())
        } else if (Global.sight == "Deuteranopia"){
            button1.setTextColor(Global.LIGHT_BLUE.toInt())
            layoutView.setBackgroundColor(Global.LIGHT_BLUE.toInt())
            textView1.setTextColor(Global.WHITE.toInt())
            textView2.setTextColor(Global.WHITE.toInt())
        } else {
            button1.setTextColor(Global.VLIGHT_PURPLE.toInt())
            layoutView.setBackgroundColor(Global.VLIGHT_PURPLE.toInt())
            textView1.setTextColor(Global.WHITE.toInt())
            textView2.setTextColor(Global.WHITE.toInt())
        }

        if (protanopia > deuteranopia)
        {
            tv_score.text = "You may have Protanopia, You got $protanopia Protanopia answers out of $totalQuestions \nSuggestion: Consult an optometrist"
            Global.sight = "Protanopia"

        }
        else if (protanopia < deuteranopia)
        {
            tv_score.text = "You may have Deuteranopia, You got $deuteranopia Deuteranopia answers out of $totalQuestions \nSuggestion: Consult an optometrist"
            Global.sight = "Deuteranopia"
        }
        else
        {
            tv_score.text = "You seem to have normal vision, You got $correctAnswers correct answers out of $totalQuestions"
            Global.sight = "Normal"
        }

        btn_finish.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
