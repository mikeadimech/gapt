package com.example.gapt.utils

import android.content.Context
import android.graphics.*
import android.view.View
import com.example.gapt.Global
import java.util.ArrayList
import kotlin.math.cos
import kotlin.math.sin

class Draw(context: Context?, var rect: Rect, var text: String): View(context) {

    lateinit var boundaryPaint: Paint
    lateinit var boundaryPaint2: Paint
    lateinit var textPaint: Paint

    init {
        init()
    }

    private fun init(){
        boundaryPaint = Paint()
        boundaryPaint.strokeWidth = 30f
        boundaryPaint.style = Paint.Style.STROKE

        boundaryPaint2 = Paint()
        boundaryPaint2.strokeWidth = 15f
        boundaryPaint2.style = Paint.Style.STROKE

        textPaint = Paint()
        textPaint.textSize = 50f
        textPaint.style = Paint.Style.FILL

        when (Global.sight) {
            "Normal" -> {
                boundaryPaint.color = Color.BLACK
                boundaryPaint2.color = Color.GREEN
                textPaint.color = Color.GREEN
            }
            "Protanopia" -> {
                boundaryPaint.color = Color.BLACK
                boundaryPaint2.color = Color.YELLOW
                textPaint.color = Color.BLACK
            }
            else -> {
                boundaryPaint.color = Color.BLACK
                boundaryPaint2.color = Color.BLUE
                textPaint.color = Color.BLUE
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawText(text, rect.centerX().toFloat(), rect.centerY().toFloat(), textPaint)
        canvas?.drawRect(
            rect.left.toFloat(),
            rect.top.toFloat(),
            rect.right.toFloat(),
            rect.bottom.toFloat(),
            boundaryPaint
        )
        canvas?.drawRect(
                rect.left.toFloat(),
                rect.top.toFloat(),
                rect.right.toFloat(),
                rect.bottom.toFloat(),
                boundaryPaint2
        )
    }
}