package com.example.gapt

object Constants {

    const val TOTAL_QUESTIONS: String = "total_questions"
    const val CORRECT_ANSWERS: String = "correct_answers"
    const val PROTANOPIA_ANSWERS: String = "protanopia_answers"
    const val DEUTERANOPIA_ANSWERS: String = "deuteranopia_answers"
    const val SIGHT: String = "sight"


    fun getQuestions(): ArrayList<Question> {
        val questionsList = ArrayList<Question>()

        val que1 = Question(1, "What do you see?", R.drawable.ic_plate_1,
                "12", "72", "2", "None", 1, -1,-1)

        val que2 = Question(2, "What do you see?", R.drawable.ic_plate_2,
                "3", "8", "9", "None", 2,1,1)

        val que3 = Question(3, "What do you see?", R.drawable.ic_plate_3,
                "5", "6", "8", "None", 2,1,1)

        val que4 = Question(4, "What do you see?", R.drawable.ic_plate_4,
                "19", "70", "29", "None", 3,2,2)

        val que5 = Question(5, "What do you see?", R.drawable.ic_plate_5,
                "35", "29", "57", "None", 3,1,1)

        val que6 = Question(6, "What do you see?", R.drawable.ic_plate_6,
                "5", "6", "2", "None", 1,3,3)

        val que7 = Question(7, "What do you see?", R.drawable.ic_plate_7,
                "5", "6", "3", "None", 3,1,1)

        val que8 = Question(8, "What do you see?", R.drawable.ic_plate_8,
                "15", "17", "16", "None", 1,2,2)

        val que9 = Question(9, "What do you see?", R.drawable.ic_plate_9,
                "21", "74", "14", "None", 2,1,1)

        val que10 = Question(10, "What do you see?", R.drawable.ic_plate_10,
                "7", "2", "9", "None", 2,4,4)

        val que11 = Question(11, "What do you see?", R.drawable.ic_plate_11,
                "6", "8", "5", "None", 1,4,4)

        val que12 = Question(12, "What do you see?", R.drawable.ic_plate_12,
                "21", "57", "97", "None", 3,4,4)

        val que13 = Question(13, "What do you see?", R.drawable.ic_plate_13,
                "45", "16", "13", "None", 1,4,4)

        val que14 = Question(14, "What do you see?", R.drawable.ic_plate_14,
                "5", "6", "3", "None", 1,4,4)

        val que15 = Question(15, "What do you see?", R.drawable.ic_plate_15,
                "2", "7", "1", "None", 2,4,4)

        val que16 = Question(16, "What do you see?", R.drawable.ic_plate_16,
                "70", "16", "18", "None", 2,4,4)

        val que17 = Question(17, "What do you see?", R.drawable.ic_plate_17,
                "18", "78", "73", "None", 3,4,4)

        val que18 = Question(18, "What do you see?", R.drawable.ic_plate_18,
                "2", "5", "6", "None", 4,2,2)

        val que19 = Question(19, "What do you see?", R.drawable.ic_plate_19,
                "2", "7", "9", "None", 4,1,1)

        val que20 = Question(20, "What do you see?", R.drawable.ic_plate_20,
                "42", "15", "45", "None", 4,3,3)

        val que21 = Question(21, "What do you see?", R.drawable.ic_plate_21,
                "15", "73", "16", "None", 4,2,2)

        val que22 = Question(22, "What do you see?", R.drawable.ic_plate_22,
                "26", "6", "2", "None", 1,2,3)

        val que23 = Question(23, "What do you see?", R.drawable.ic_plate_23,
                "4", "2", "42", "None", 3,2,1)

        val que24 = Question(24, "What do you see?", R.drawable.ic_plate_24,
                "35", "3", "5", "None", 1,3,2)

        val que25 = Question(25, "What do you see?", R.drawable.ic_plate_25,
                "6", "96", "9", "None", 2,1,3)

        val que26 = Question(26, "What do you see?", R.drawable.ic_plate_26,
                "Red Line", "Purple Line", "Both", "None", 3,2,1)

        val que27 = Question(27, "What do you see?", R.drawable.ic_plate_27,
                "Red Line", "Purple Line", "Both", "None", 3,2,1)

        val que28 = Question(28, "What do you see?", R.drawable.ic_plate_28,
                "Line", "Square", "Circle", "None", 4,1,1)

        val que29 = Question(29, "What do you see?", R.drawable.ic_plate_29,
                "Line", "Triangle", "Circle", "None", 4,1,1)

        val que30 = Question(30, "What do you see?", R.drawable.ic_plate_30,
                "Green Line", "Blue Star", "Green Triangle", "None", 1,4,4)

        val que31 = Question(31, "What do you see?", R.drawable.ic_plate_31,
                "Green Line", "Green Star", "Blue Square", "None", 1,4,4)

        val que32 = Question(32, "What do you see?", R.drawable.ic_plate_32,
                "Orange Line", "Green Circle", "Red Circle", "None", 1,4,4)

        val que33 = Question(33, "What do you see?", R.drawable.ic_plate_33,
                "Orange Line", "Square", "2 Squares", "None", 1,4,4)

        val que34 = Question(34, "What do you see?", R.drawable.ic_plate_34,
                "Pink Line", "Green Line", "Red Line", "Violet Line", 2,4,4)

        val que35 = Question(35, "What do you see?", R.drawable.ic_plate_35,
                "Green Line", "Red Line", "Violet Line", "None", 1,4,4)

        val que36 = Question(36, "What do you see?", R.drawable.ic_plate_36,
                "Blue-green Line", "Orange Line", "Violet Line", "None", 2,4,4)

        val que37 = Question(37, "What do you see?", R.drawable.ic_plate_37,
                "Blue-green Line", "Orange Line", "Violet Line", "None", 2,4,4)

        val que38 = Question(38, "What do you see?", R.drawable.ic_plate_38,
                "Orange Line", "Orange Square", "Orange Triangle", "None", 1,-1,-1)

        questionsList.add(que1)
        questionsList.add(que2)
        questionsList.add(que3)
        questionsList.add(que4)
        questionsList.add(que5)
        questionsList.add(que6)
        questionsList.add(que7)
        questionsList.add(que8)
        questionsList.add(que9)
        questionsList.add(que10)
        questionsList.add(que11)
        questionsList.add(que12)
        questionsList.add(que13)
        questionsList.add(que14)
        questionsList.add(que15)
        questionsList.add(que16)
        questionsList.add(que17)
        questionsList.add(que18)
        questionsList.add(que19)
        questionsList.add(que20)
        questionsList.add(que21)
        questionsList.add(que22)
        questionsList.add(que23)
        questionsList.add(que24)
        questionsList.add(que25)
        questionsList.add(que26)
        questionsList.add(que27)
        questionsList.add(que28)
        questionsList.add(que29)
        questionsList.add(que30)
        questionsList.add(que31)
        questionsList.add(que32)
        questionsList.add(que33)
        questionsList.add(que34)
        questionsList.add(que35)
        questionsList.add(que36)
        questionsList.add(que37)
        questionsList.add(que38)

        return questionsList
    }
}