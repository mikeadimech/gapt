package com.example.gapt
class Global {
    companion object Global {
        var sight: String? = "Normal"

        val LIGHT_YELLOW = 0xFFE8F086
        val BLACK = 0xFF000000
        val LIGHT_BLUE = 0xFF235FA4
        val WHITE = 0xFFFFFFFF
        val DARK_PURPLE = 0XFF6A1B9A
        val VLIGHT_PURPLE = 0xFF8E75FF
    }
}