package com.example.gapt

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        val layoutView = findViewById<LinearLayout>(R.id.container)
        val button1 = findViewById<Button>(R.id.btn_ObjectDetection)
        val button2 = findViewById<Button>(R.id.btn_ColorDetection)
        val textView1 = findViewById<TextView>(R.id.tv_app_name)

        if(Global.sight == "Protanopia"){
            layoutView.setBackgroundColor(Global.LIGHT_YELLOW.toInt())
            button1.setBackgroundColor(Global.LIGHT_YELLOW.toInt())
            button1.setTextColor(Global.BLACK.toInt())
            button2.setBackgroundColor(Global.LIGHT_YELLOW.toInt())
            button2.setTextColor(Global.BLACK.toInt())
            textView1.setTextColor(Global.BLACK.toInt())
        } else if (Global.sight == "Deuteranopia"){
            layoutView.setBackgroundColor(Global.LIGHT_BLUE.toInt())
            button1.setBackgroundColor(Global.LIGHT_BLUE.toInt())
            button1.setTextColor(Global.WHITE.toInt())
            button2.setBackgroundColor(Global.LIGHT_BLUE.toInt())
            button2.setTextColor(Global.WHITE.toInt())
            textView1.setTextColor(Global.WHITE.toInt())
        } else {
            layoutView.setBackgroundColor(Global.VLIGHT_PURPLE.toInt())
            button1.setBackgroundColor(Global.DARK_PURPLE.toInt())
            button1.setTextColor(Global.WHITE.toInt())
            button2.setBackgroundColor(Global.DARK_PURPLE.toInt())
            button2.setTextColor(Global.WHITE.toInt())
            textView1.setTextColor(Global.WHITE.toInt())
        }

        val buttonOne = findViewById<Button>(R.id.btn_ObjectDetection)
        buttonOne.setOnClickListener {
            val intent = Intent(this, ObjectDetectionActivity::class.java)

            startActivity(intent)
            finish()
        }

        val buttonTwo = findViewById<Button>(R.id.btn_ColorDetection)
        buttonTwo.setOnClickListener {
            checkPermission(Manifest.permission.CAMERA,100)
        }

        val buttonThree = findViewById<Button>(R.id.btn_Quiz)
        buttonThree.setOnClickListener {
            val intent = Intent(this, QuizQuestionsActivity::class.java)

            startActivity(intent)
            finish()
        }

        val buttonFour = findViewById<Button>(R.id.btn_Settings)
        buttonFour.setOnClickListener {
            Toast.makeText(applicationContext,
                    "Access colour settings from this page",
                    Toast.LENGTH_LONG)
                    .show()

            startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))

            finish()
        }
    }

    // Function to check and request permission.
    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this@MainActivity, permission) == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(permission), requestCode)
        } else {
            val intent = Intent(this, ColorDetectionActivity::class.java)

            startActivity(intent)
            finish()
        }
    }

    // This function is called when the user accepts or decline the permission.
    // Request Code is used to check which permission called this function.
    // This request code is provided when the user is prompt for permission.
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@MainActivity, "Camera Permission Granted", Toast.LENGTH_SHORT).show()

                val intent = Intent(this, ColorDetectionActivity::class.java)

                startActivity(intent)
                finish()
            } else {
                Toast.makeText(this@MainActivity, "Camera Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }
}